This is a stand alone module that interfaces with Recurly.

-------------------------
Basic Setup Instructions
-------------------------
- set your Recurly API setting at admin/settings/recurly
- set up the Recurly permissions at admin/user/permissions
  - authenticated user usually have "edit subscription plans" and "view own recurly account" checked


This module development has been sponsored by Duedil