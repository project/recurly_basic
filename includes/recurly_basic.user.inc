<?php
/**
 * @file
 * Recurly settings forms and user page callbacks.
 */

/**
 * Displays a list of subscription plans currently defined in your Recurly account.
 */
function recurly_basic_subscription_plans_overview($uid = 0) {
  // Initialize the Recurly client with the site-wide settings.
  if (!recurly_basic_client_initialize()) {
    return t('Could not initialize the Recurly client.');
  }

  // Retrieve the plans for the current account.
  $plans = recurly_basic_subscription_plans();

  // Format the plan data into a table for display.
  $header = array(t('Subscription plan'), t('Price'), t('Setup fee'), t('Trial'), t('Created'), t('Operations'));
  $rows = array();

  foreach ($plans as $plan) {
    $operations = array();
    $description = '';

    // Prepare the description string if one is given for the plan.
    if (!empty($plan->description)) {
      $description = '<div class="description">' . check_plain($plan->description) . '</div>';
    }

    // Add an edit link if available for the current user.
    if (user_access('edit subscription plans')) {
      $operations[] = array(
        'title' => t('edit'),
        'href' => recurly_basic_subscription_plan_edit_url($plan) . '/edit',
        'html' => TRUE,
      );
    }
    // Get the user object
    $recurly_account = recurly_basic_account_load(array('uid' => $uid));
    $user = user_load(array('uid' => $uid));

    // Add a purchase link if Hosted Payment Pages are enabled.
    if (variable_get('recurly_hosted_payment_pages', FALSE)) {
      $operations[] = array(
        'title' => t('purchase'),
        'href' => recurly_basic_subscription_plan_purchase_url($plan) . recurly_basic_subscription_plan_paramters($user),
        'html' => TRUE,
      );
    }

    $rows[] = array(
      t('@name <small>(Plan code: @code)</small>', array('@name' => $plan->name, '@code' => $plan->plan_code)) . $description,
      t('@unit_price per @interval_length @interval_unit', array('@unit_price' => number_format($plan->unit_amount_in_cents / 100, 2), '@interval_length' => $plan->plan_interval_length, '@interval_unit' => $plan->plan_interval_unit)),
      t('@setup_fee', array('@setup_fee' => number_format($plan->setup_fee_in_cents / 100, 2))),
      t('@trial_length @trial_unit', array('@trial_length' => $plan->trial_interval_length, '@trial_unit' => $plan->trial_interval_unit)),
      format_date($plan->created_at),
      theme_links($operations, array('links', 'inline')),
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No subscription plans found.'), 'colspan' => 5));
  }

  return theme('table',$header, $rows);
}

/**
 * Hook recurly_subscription_plan_paramters.
 *
 * To implement this hook, alter the parameter that is passed by reference
 * this allows you to inject additional parameters to customers recurly subscription form.
 * A key value substitution, so the array key should match the parameter name set for recurly.
 * 
 * You may populate the subscription form by setting the keys first_name, last_name, and email parameters.
 *
 * @see http://docs.recurly.com/hosted-payment-pages/integration/ for additional parameters.
 *
 * @param $user 
 *  The user we are adding parameters to.
 */
function recurly_basic_subscription_plan_paramters($user) {
  if (!isset($user) || $user->uid = 0) {
    return;
  }
  $parameters = array(
    'email' => $user->mail,
    'first_name' => '',
    'last_name' => '',
  );
  // Hook to allow other modules to inject information into fields.
  module_invoke_all('recurly_basic_subscription_plan_paramters', $user, &$parameters);

  // Now pars and return paramaters
  return '?' . http_build_query($parameters);
}
