<?php

/**
 * @file
 * Recurly settings forms and administration page callbacks.
 */


/**
 * Returns the site-wide Recurly settings form.
 */
function recurly_basic_settings_form() {
  $form = array();
  // Add form elements to collect default account information.
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default account settings'),
    '#description' => t('Configure these settings based on your Company Settings and API Credentials settings in the Recurly administration interface.'),
    '#collapsible' => TRUE,
  );
  $form['account']['recurly_subdomain'] = array(
    '#type' => 'textfield',
    '#title' => t('Subdomain'),
    '#description' => t("The subdomain of your account excluding the -test suffix if still in Sandbox."),
    '#default_value' => variable_get('recurly_subdomain', ''),
  );
  $form['account']['recurly_hosted_payment_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hosted Payment Pages are enabled for this account.'),
    '#default_value' => variable_get('recurly_hosted_payment_pages', FALSE),
  );
  $form['account']['recurly_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('API username'),
    '#default_value' => variable_get('recurly_api_username', ''),
  );
  $form['account']['recurly_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('API password'),
    '#default_value' => variable_get('recurly_api_password', ''),
  );
  $form['account']['recurly_environment'] = array(
    '#type' => 'radios',
    '#title' => t('Environment'),
    '#description' => t('Select the environment that matches your account status.'),
    '#options' => array(
      'sandbox' => t('Sandbox'),
      'production' => t('Production'),
    ),
    '#default_value' => variable_get('recurly_environment', 'sandbox'),
  );

  // Add form elements to configure default push notification settings.
  $form['push'] = array(
    '#type' => 'fieldset',
    '#title' => t('Push notification settings'),
    '#description' => t('If you have supplied an HTTP authentication username and password in your Push Notifications settings at Recurly, your web server must be configured to validate these credentials at your listener URL.'),
    '#collapsible' => TRUE,
  );
  $form['push']['duedil_recurly_listener_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Listener URL key'),
    '#description' => t('Customizing the listener URL gives you protection against fraudulent push notifications.') . '<br />' . t('Based on your current key, you should set @url as your Push Notification URL at Recurly.', array('@url' => url('recurly/listener/' . variable_get('recurly_listener_key', ''), array('absolute' => TRUE)))),
    '#default_value' => variable_get('recurly_listener_key', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#field_prefix' => url('recurly/listener/', array('absolute' => TRUE)),
  );
  $form['push']['recurly_push_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log authenticated incoming push notifications. (Primarily used for debugging purposes.)'),
    '#default_value' => variable_get('recurly_push_logging', FALSE),
  );

  // Add form elements allowing the administrator to toggle integration options.
  $form['integration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Integration options'),
    '#collapsible' => TRUE,
  );
  $form['integration']['recurly_account_integration'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Account integration'),
    '#options' => array(
      'profile_display' => t('Display Recurly account information on user profile pages, including account management links if Hosted Payment Pages are enabled.'),
      'push_create' => t('Create local account records upon receipt of push notifications linked to users by e-mail address. For maximum security, this should only be enabled if e-mail account verification is required during registration.'),
      'push_update' => t('Update local account records upon receipt of push notifications. This option can be used without creating local account records on notifications, but if you are, you should most likely be updating them, too.'),
    ),
    '#default_value' => variable_get('recurly_account_integration', array()),
  );
  
  return system_settings_form($form);
}


/**
 * Displays a list of users cached locally.
 */
function recurly_basic_subscription_local_users() {

  // Initialize the Recurly client with the site-wide settings.
  if (!recurly_basic_client_initialize()) {
    return t('Could not initialize the Recurly client.');
  }
  
  // Retrieve the plans for the current account.
  $plans = recurly_basic_subscription_plans();

  // Format the plan data into a table for display.
  $header = array(t('User'), t('Status'), t('Updated'), t('Account Code'), t('Email'), t('First Name'), t('Last Name'), t('Company Name'),  t('Edit'));
  $rows = array();
  $query = db_query("SELECT * FROM {recurly_account}");
  while($account = db_fetch_object($query)) {
    $user = user_load(array('uid' => $account->uid));
    $recurly_account = recurly_basic_account_load(array('uid' => $account->uid));
    $rows[] = array(
      l($user->name, 'user/' . $account->uid),
      $account->status,
      format_date($account->updated, 'custom', 'H:m:s d M j'),
      $account->account_code,
      $account->email,
      $account->first_name,
      $account->last_name,
      $account->company_name,
      l(t('Edit this account at Recurly.'), recurly_basic_account_edit_url($recurly_account)),
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No subscribed users found.'), 'colspan' => 9));
  }

  return theme('table',$header, $rows);
}